FROM ocaml/opam2:latest

RUN sudo apt install -y m4
RUN opam update
RUN opam install -y cohttp cohttp-lwt-unix
ADD . .
CMD eval $(opam env) && cohttp-server-lwt