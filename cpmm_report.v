Require List. (* .none *)
Require Import String. (* .none *)
Require Import NArith. (* .none *)
Open Scope string_scope. (* .none *)
Import List.ListNotations. (* .none *)
Open Scope list_scope. (* .none *)
Require Import ZArith. (* .none *)
Require Import Lia. (* .none *)
Require Import Michocoq.macros. (* .none *)
Import syntax. (* .none *)
Import comparable. (* .none *)
Require Import Michocoq.util. (* .none *)
Import error. (* .none *)
Require Import contract_semantics. (* .none *)
Require cpmm_definition. (* .none *)
Require Import cpmm_storage. (* .none *)
Require Import cpmm_spec.
Require Import cpmm_verification_ep_addLiquidity.
Require Import cpmm_verification_ep_removeLiquidity.
Require Import cpmm_verification_ep_default.
Require Import cpmm_verification_ep_tokenToXtz.
Require Import cpmm_verification_ep_xtzToToken.
Require Import cpmm_verification_ep_tokenToToken.

(*|

=====================================================================
Verification of the Constant Product Market Maker of Liquidity Baking
=====================================================================

.. _compilation-correctness:

Correctness of compilation
--------------------------

addLiquidity
^^^^^^^^^^^^

``cpmm_spec.ep_addLiquidity`` is the high-level specification of ``addLiquidity``
and ``ep_addLiquidity`` is the Michelson code for this entrypoint.

|*)

Print cpmm_spec.ep_addLiquidity. (* .fold *)

Print ep_addLiquidity. (* .fold *)

(*|

We show that they are semantically equivalent.

|*)

Check ep_addLiquidity_correct. (* .unfold .messages *)

(*|

removeLiquidity
^^^^^^^^^^^^^^^

``cpmm_spec.ep_removeLiquidity`` is the high-level specification of ``removeLiquidity``
and ``ep_removeLiquidity`` is the Michelson code for this entrypoint.

|*)

Print cpmm_spec.ep_removeLiquidity. (* .fold *)

Print ep_removeLiquidity. (* .fold *)

(*|

We show that they are semantically equivalent.

|*)

Check ep_removeLiquidity_correct. (* .unfold .messages *)

(*|

tokenToXtz
^^^^^^^^^^

``cpmm_spec.ep_tokenToXtz`` is the high-level specification of ``tokenToXtz``
and ``ep_tokenToXtz`` is the Michelson code for this entrypoint.

|*)

Print cpmm_spec.ep_tokenToXtz. (* .fold *)

Print ep_tokenToXtz. (* .fold *)

(*|

We show that they are semantically equivalent.

|*)

Check ep_tokenToXtz_correct. (* .unfold .messages *)

(*|

xtzToToken
^^^^^^^^^^

``cpmm_spec.ep_xtzToToken`` is the high-level specification of ``xtzTotoken``
and ``ep_xtzToToken`` is the Michelson code for this entrypoint.

|*)

Print cpmm_spec.ep_xtzToToken. (* .fold *)

Print ep_xtzToToken. (* .fold *)

(*|

We show that they are semantically equivalent.

|*)

Check ep_xtzToToken_correct. (* .unfold .messages *)

(*|

tokenToToken
^^^^^^^^^^^^

``cpmm_spec.ep_tokenToToken`` is the high-level specification of ``tokenToToken``
and ``ep_tokenToToken`` is the Michelson code for this entrypoint.

|*)

Print cpmm_spec.ep_tokenToToken. (* .fold *)

Print ep_tokenToToken. (* .fold *)

(*|

We show that they are semantically equivalent.

|*)

Check ep_tokenToToken_correct. (* .unfold .messages *)

(*|

default
^^^^^^^

``cpmm_spec.ep_default`` is the high-level specification of ``default``
and ``ep_default`` is the Michelson code for this entrypoint.

|*)

Print cpmm_spec.ep_default. (* .fold *)

Print ep_default. (* .fold *)

(*|

We show that they are semantically equivalent.

|*)

Check ep_default_correct. (* .unfold .messages *)

(*|

.. _safety:

Safety of execution
-------------------

The following invariant is sufficient to ensure that the CPMM will
neither get stuck nor issue a division by zero.

|*)

Print cpmm_spec.arithmetic_invariant. (* .unfold .messages *)

(*|

where ``product_lower_bound`` is defined as follows:

|*)
Print product_lower_bound. (* .unfold .messages *)

(*|

We show that each entrypoint preserves this invariant for all entrypoints
except ``removeLiquidity``.

|*)

Check addLiquidity_safe. (* .unfold .messages *)
Check tokenToToken_safe. (* .unfold .messages *)
Check xtzToToken_safe.   (* .unfold .messages *)
Check tokenToToken_safe. (* .unfold .messages *)
Check tokenToXtz_safe.   (* .unfold .messages *)

(*|

Indeed, ``removeLiquidity`` needs an extra external hypothesis: nobody
can empty the liquidity. This is guaranteed externally by an initial
deposit assigned to the null address.

|*)

Check removeLiquidity_safe. (* .unfold .messages *)

(*|

where

|*)

Print ep_removeLiquidity_with_external_assumption_about_liquidity_removal_preserves_invariant. (* .unfold .messages *)

Print ep_removeLiquidity_with_external_assumption_about_liquidity_removal. (* .unfold .messages *)

(*|

.. _product:

Evolution of the product of supplies
------------------------------------



|*)
